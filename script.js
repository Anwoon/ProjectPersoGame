$(document).ready(function () {

  document.getElementById("scoreMaximum").innerHTML = "Score maximum: " + localStorage.getItem('score');

  $("#play").on("click",nomDuJoueur);

  var player;

  function nomDuJoueur(){
    player =  document.getElementById("player").value;
    document.getElementById("Player").innerHTML = player;
    document.getElementById("formPlayer").style.display = "none";
    GameStart();
  }

  function GameStart(){

  //NOS VARIABLES
  var floppy = document.querySelector('#floppy');
  var $enemy_up = $('#enemy_up');
  var $enemy_down = $('#enemy_down');
  var score= 0;
  var started = false; //par defaut
  var ay = -.6; // accélération (gravité)
  var vy = 1; // vitesse
  var y = 0; // position

  function Animation () {

  $enemy_up.css({
    "animation" : "moveSlideshow 3s linear infinite"
  })

  $enemy_down.css({
    "animation": "moveSlideshow 3s linear infinite"
  })

  setInterval(function () {
    setInterval(function () {
      $enemy_up.css({
        "height": "80px"
      })
      $enemy_down.css({
        "height": "220px"
      })
    }, 3000);

    setInterval(function () {
      $enemy_up.css({
        "height": "220px"
      })
      $enemy_down.css({
        "height": "80px"
      })
    }, 6000);

    setInterval(function () {
      $enemy_up.css({
        "height": "160px"
      })
      $enemy_down.css({
        "height": "160px"
      })
    }, 9000);
  }, 12000);
 }

 Animation();

  // loop
  var refreshFloppy = setInterval(refresh, 10);
  setInterval(refreshColision,200);
  var scoreRefresh = setInterval(refreshScore,1000);

  // move
  function refresh() {
    updatePhysics();
    updateView();
  }

  function refreshColision(){
    checkColisionDown();
    checkColisionUp();
  }

  function refreshScore(){
    scores();
  }

  function updatePhysics() {
    if (started == false) { return; } //start false par default (n'affiche rien)
    if (y < 240) {
      y += vy; // modifier la position
      vy += ay; // modifier la vitesse
      y = Math.max(-230, y); // pour éviter les valeurs négatives
    }
    else {
      for (y; y >= 240; y--) { }
    }
  }
  function updateView() {
    floppy.style.bottom = 320 + y + 'px';
  }
  //click pour faire sauter l'oiseau
    document.addEventListener('click', jump);
    document.addEventListener('keypress', jump);

  function jump() {

    started = true;// premier click on start le jeu
    vy = 8; // vitesse vers le haut
  }

  function scores (){
    
    score++
    document.getElementById("Score").innerHTML = "Score:" + " " + score;
  }

  function perdu () {
    document.getElementById("Perdu").innerHTML = player + " " + "tu a perdu avec un score de" + " " + score;

    if(localStorage.getItem('score')< score){
      localStorage.setItem('score', score);
    }
    $enemy_up.css({
      "animation" : "none"
    })

    $enemy_down.css({
      "animation": "none"
    })
      clearInterval(refreshFloppy);

      clearInterval(scoreRefresh);

    setTimeout(function() {

    location.reload();

    },3000)
  }

  var intervalcolisionBottom = setInterval(colisionBottom,150);

    function colisionBottom () {
    if(y == -230){

      clearInterval(intervalcolisionBottom);

      perdu();
    }
  }

  function checkColisionUp () {
    var boum = false;

    $('#floppy').each(function () {
      if (colision($('#enemy_up'), $(this))) {

        boum = true;

        perdu();

        return;
      }
    });

    return boum;
  }

  function checkColisionDown () {
    var boum = false;
    $('#floppy').each(function () {
      if (colision($('#enemy_down'), $(this))) {

        boum = true;

        perdu();

        return;
      }
    });

    return boum;
  }

  function colision (elt1, elt2) {

    var p_elt1 = elt1.position();
    var p_elt2 = elt2.position();

    if (p_elt1.left < p_elt2.left + elt2.width() &&
       p_elt1.left + elt1.width() > p_elt2.left &&
       p_elt1.top < p_elt2.top + elt2.height() &&
       elt1.height() + p_elt1.top > p_elt2.top) {
        return true;
    }
  }
}
});